/** @type {import('tailwindcss').Config} */

const config = {
  content: ['./src/**/*.{html,js}'],
  theme: {
    extend: {}
  },
  plugins: ['prettier-plugin-tailwindcss']
};

module.exports = config;
