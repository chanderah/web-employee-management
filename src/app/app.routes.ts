import { Routes } from '@angular/router';
import { LoginComponent } from './component/auth/login/login.component';
import { authChildGuard, authGuard } from './guard/auth.guard';

export const routes: Routes = [
  {
    path: 'auth',
    children: [{ path: 'login', component: LoginComponent }]
  },
  {
    path: 'employee',
    canActivate: [authGuard],
    canActivateChild: [authChildGuard],
    loadChildren: () => import('./component/employee/employee.routes').then((r) => r.employeeRoutes)
  },
  { path: '**', redirectTo: 'employee', pathMatch: 'full' }
];
