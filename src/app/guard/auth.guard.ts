import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateChildFn, CanActivateFn, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../service/auth.service';

const isAuthenticated = (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
  const router = inject(Router);
  const authService = inject(AuthService);

  const user = authService.getUser();
  if (!user) {
    router.navigateByUrl('/auth/login');
    return false;
  }
  return true;
};

export const authGuard: CanActivateFn = isAuthenticated;
export const authChildGuard: CanActivateChildFn = isAuthenticated;
