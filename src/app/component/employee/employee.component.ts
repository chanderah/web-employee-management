import { CommonModule } from '@angular/common';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { Router, RouterModule } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ComponentState } from '../../interface/component_state';
import { Employee, EmployeeFilter } from '../../interface/employee';
import { FormType } from '../../interface/form';
import { PagingInfo } from '../../interface/paging_info';
import { CustomCurrencyPipe } from '../../pipe/custom-currency.pipe';
import { MockApiService } from '../../service/mock-api.service';

@Component({
  selector: 'app-employee',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    MatTableModule,
    MatPaginatorModule,
    MatDividerModule,
    MatSortModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatProgressBarModule,
    CustomCurrencyPipe
    // CustomCurrencyPipe
  ],
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  isLoading: boolean = true;
  state!: ComponentState<Employee>;

  pageSizeOptions: number[] = [10, 20, 50, 100];
  pagingInfo: PagingInfo<EmployeeFilter> = {
    filter: { username: '', group: '' } as EmployeeFilter,
    limit: this.pageSizeOptions[0],
    page: 0,
    sortField: 'id',
    sortOrder: 'asc'
  };

  // dateColumns: string[] = ['birthDate', 'description'];
  displayedColumns: string[] = ['action', 'id', 'username', 'firstName', 'lastName', 'email', 'birthDate', 'basicSalary', 'status', 'group', 'description'];
  dataSource = new MatTableDataSource<Employee>();
  employees = [] as Employee[];

  filter: FormGroup = this.fb.group({
    username: [''],
    group: ['']
  });

  constructor(
    private apiService: MockApiService,
    private fb: FormBuilder,
    private toastService: ToastrService,
    private router: Router
  ) {
    this.state = router.getCurrentNavigation()?.previousNavigation?.extras.state as ComponentState<Employee>;
  }

  ngOnInit(): void {
    if (this.state?.formType !== FormType.ADD && this.state?.history?.pagingInfo) {
      this.pagingInfo = this.state?.history?.pagingInfo;
      this.filter.patchValue(this.state?.history?.pagingInfo.filter);
    }

    this.filter.valueChanges.subscribe((input) => this.onFilterChange(input));
    setTimeout(() => this.getEmployees(), 1000);
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;

    this.sort.sortChange.subscribe(({ active, direction }) => {
      this.pagingInfo.sortField = active;
      this.pagingInfo.sortOrder = direction;
      this.pagingInfo.page = 0;
      this.getEmployees();
    });

    this.paginator.page.subscribe(({ pageIndex, pageSize }) => {
      this.pagingInfo.page = pageIndex;
      this.pagingInfo.limit = pageSize;
      this.getEmployees();
    });
  }

  getEmployees() {
    this.isLoading = true;
    this.apiService.getEmployeesPaging(this.pagingInfo).subscribe((res) => {
      this.isLoading = false;
      this.dataSource.data = res.data;

      setTimeout(() => {
        this.paginator.length = res.rowCount;
        this.paginator.pageIndex = this.pagingInfo.page;
      });
    });
  }

  onFilterChange({ username, group }: Employee) {
    this.pagingInfo.filter = { username, group };
    this.pagingInfo.page = 0;
    this.getEmployees();
  }

  onAddEmployee() {
    this.router.navigate(['/employee/details'], {
      state: {
        formType: FormType.ADD,
        data: {}
      } as ComponentState<Employee>
    });
  }

  onViewEmployee(data: Employee) {
    this.router.navigate(['/employee/details'], {
      state: {
        formType: FormType.VIEW,
        data,
        history: {
          url: this.router.url,
          pagingInfo: JSON.parse(JSON.stringify(this.pagingInfo))
        }
      } as ComponentState<Employee>
    });
  }

  onEditEmployee(e: MouseEvent, data: Employee) {
    e.stopPropagation();
    this.toastService.warning(`onEdit ${data.username}`);
  }

  onDeleteEmployee(e: MouseEvent, data: Employee) {
    e.stopPropagation();
    this.toastService.error(`onDelete ${data.username}`);
  }
}
