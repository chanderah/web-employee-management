import { Route } from '@angular/router';

export const employeeRoutes: Route[] = [
  {
    path: '',
    loadComponent: () => import('./employee.component').then((c) => c.EmployeeComponent)
  },
  {
    path: 'details',
    loadComponent: () => import('./employee-details/employee-details.component').then((c) => c.EmployeeDetailsComponent)
  }
];
