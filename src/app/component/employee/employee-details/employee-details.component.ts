import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';

import { Location } from '@angular/common';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatOptionModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { Router, RouterModule } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ComponentState } from '../../../interface/component_state';
import { Employee } from '../../../interface/employee';
import { EmployeeGroup } from '../../../interface/employee_group';
import { FormType } from '../../../interface/form';
import { CustomCurrencyPipe } from '../../../pipe/custom-currency.pipe';
import { MockApiService } from '../../../service/mock-api.service';

@Component({
  selector: 'app-employee-details',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    MatDividerModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatDatepickerModule,
    MatSelectModule,
    MatOptionModule,
    MatAutocompleteModule,
    CustomCurrencyPipe
  ],
  templateUrl: './employee-details.component.html',
  styleUrl: './employee-details.component.scss'
})
export class EmployeeDetailsComponent implements OnInit {
  state!: ComponentState<Employee>;

  formType: FormType = FormType.ADD;
  currentDate: Date = new Date();

  employeeGroups = [] as EmployeeGroup[];
  filteredEmployeeGroups = [] as EmployeeGroup[];

  formErrors: string[] = [];
  form: FormGroup = this.fb.group({
    id: [0, Validators.required],
    username: ['', Validators.required],
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    birthDate: [null, Validators.required],
    basicSalary: [null, Validators.required],
    status: ['', Validators.required],
    group: ['', Validators.required],
    description: [null, Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private location: Location,
    private apiService: MockApiService,
    private toastService: ToastrService
  ) {
    this.state = router.getCurrentNavigation()?.extras.state as ComponentState<Employee>;
  }

  ngOnInit() {
    if (this.state?.formType === FormType.VIEW) {
      this.formType = this.state.formType;
      this.form.patchValue(this.state.data);
      this.form.disable();
    } else {
      this.employeeGroups = this.apiService.getEmployeeGroups();
      this.filteredEmployeeGroups = this.employeeGroups.slice();

      this.form.get('group')?.valueChanges.subscribe((input: string) => {
        this.filteredEmployeeGroups = this.employeeGroups.filter(
          (filtered) => filtered.name.toString().toLowerCase().indexOf(input.toString().trim().toLowerCase()) > -1
        );
      });
    }
  }

  async onSubmit() {
    const err = await this.formHasErrors();
    if (err) return;

    const employee: Employee = this.form.value;
    this.apiService.addEmployees(employee).subscribe(() => {
      this.router.navigateByUrl('/employee').then(() => {
        this.toastService.success('Employee is added!');
      });
    });
  }

  onCancel() {
    this.location.back();
  }

  onBlurGroup() {
    const selected = this.employeeGroups.find((v) => v.name === this.form.get('group')?.value);
    if (!selected) this.form.get('group')?.setValue('');
  }

  formHasErrors(): Promise<boolean> {
    return new Promise((resolve) => {
      this.formErrors.length = 0;
      Object.keys(this.form.controls).forEach((key) => {
        if (this.form.controls[key].status === 'INVALID') this.formErrors.push(key);
      });
      resolve(this.formErrors.length > 0 ? true : false);
    });
  }
}
