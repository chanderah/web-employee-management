import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppComponent } from '../../../app.component';
import { User } from '../../../interface/user';
import { AuthService } from '../../../service/auth.service';
import { MockApiService } from '../../../service/mock-api.service';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, MatFormFieldModule, MatInputModule, MatIconModule, MatButtonModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent implements OnInit {
  formErrors: string[] = [];
  form: FormGroup = this.fb.group({
    username: ['chandra', Validators.required],
    password: ['developer', Validators.required]
  });

  constructor(
    private app: AppComponent,
    private fb: FormBuilder,
    private router: Router,
    private apiService: MockApiService,
    private authService: AuthService,
    private toastService: ToastrService
  ) {}

  ngOnInit(): void {
    if (this.app.isLoggedIn) this.router.navigateByUrl('/');
  }

  async onSubmit() {
    const err = await this.formHasErrors();
    if (err) return;

    this.authService.login(this.form.value as User).subscribe((authenticated) => {
      if (authenticated) {
        this.router.navigateByUrl('/');
      } else this.toastService.error('Failed to authenticate!');
    });
  }

  formHasErrors(): Promise<boolean> {
    return new Promise((resolve) => {
      this.formErrors.length = 0;
      Object.keys(this.form.controls).forEach((key) => {
        if (this.form.controls[key].status === 'INVALID') this.formErrors.push(key);
      });
      resolve(this.formErrors.length > 0 ? true : false);
    });
  }
}
