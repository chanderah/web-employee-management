import { FormType } from './form';
import { PagingInfo } from './paging_info';

export interface ComponentState<T> {
  formType: FormType;
  data: T;
  history?: {
    pagingInfo: PagingInfo<T>;
  };
}
