import { Employee } from './employee';
import { EmployeeGroup } from './employee_group';

export interface DummyData {
  groups: EmployeeGroup[];
  employees: Employee[];
}
