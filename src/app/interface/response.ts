export interface MockResponse<T> {
  status: number;
  data: T;
  message?: string;
  rowCount: number;
}
