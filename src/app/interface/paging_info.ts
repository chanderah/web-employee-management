export interface PagingInfo<T> {
  filter: T;
  limit: number | 10 | 25 | 50 | 100;
  page: number;
  offset?: number;
  sortField: string;
  sortOrder: 'asc' | 'desc' | '';
  // rowCount: number;
}
