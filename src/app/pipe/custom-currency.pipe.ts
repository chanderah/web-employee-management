import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'custom',
  standalone: true
})
export class CustomCurrencyPipe implements PipeTransform {
  transform(val: string) {
    return new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
      minimumFractionDigits: 2
    })
      .format(Number(val))
      .replace('Rp', 'Rp.');
  }
}
