import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { DummyData } from '../interface/dummy';
import { EmployeeFilter } from '../interface/employee';
import { EmployeeGroup } from '../interface/employee_group';
import { PagingInfo } from '../interface/paging_info';
import { MockResponse } from '../interface/response';
import { Employee } from './../interface/employee';

@Injectable({
  providedIn: 'root'
})
export class MockApiService {
  constructor(private httpClient: HttpClient) {}

  async initData() {
    const employees = this.getEmployees();
    const groups = this.getEmployeeGroups();

    if (employees.length === 0 || groups.length === 0) {
      const dummy: DummyData = this.getDummyData();
      this.setEmployeeGroups(dummy.groups);
      this.setEmployee(dummy.employees);
    }
  }

  getEmployees(): Employee[] {
    const employees = localStorage.getItem('employees');
    return employees ? this.decode(employees) : [];
  }

  getEmployeesPaging(pagingInfo: PagingInfo<EmployeeFilter>): Observable<MockResponse<Employee[]>> {
    let employees = this.getEmployees();
    employees = employees
      .sort((a: any, b: any) => {
        const data1 = a[pagingInfo.sortField].toString().replace(/[A-Za-z$-]/g, '');
        const data2 = b[pagingInfo.sortField].toString().replace(/[A-Za-z$-]/g, '');
        return pagingInfo.sortOrder === 'asc' ? data1 - data2 : data2 - data1;
      })
      .filter(
        (v) =>
          v.username.toLowerCase().includes(pagingInfo.filter.username.toLowerCase()) && v.group.toLowerCase().includes(pagingInfo.filter.group.toLowerCase())
      );

    const rowCount = employees.length;
    const from = pagingInfo.page * pagingInfo.limit;
    const to = from + pagingInfo.limit;

    return of({
      status: 200,
      message: 'success',
      data: employees.slice(from, to),
      rowCount: rowCount
    });
  }

  setEmployee(employees: Employee[]) {
    localStorage.setItem('employees', this.encode(employees));
  }

  getEmployeeGroups(): EmployeeGroup[] {
    const groups = localStorage.getItem('groups');
    return groups ? this.decode(groups) : [];
  }

  setEmployeeGroups(groups: EmployeeGroup[]) {
    localStorage.setItem('groups', this.encode(groups));
  }

  addEmployees(employee: Employee): Observable<boolean> {
    const data = this.getEmployees();
    this.setEmployee([
      ...data,
      {
        ...employee,
        id: this.getLatestEmployeeId() + 1
      }
    ]);
    return of(true);
  }

  getLatestEmployeeId() {
    const employee = this.getEmployees();
    return Math.max(...employee.map((v) => v.id));
  }

  encode(data: any): string {
    if (typeof data !== 'string') data = JSON.stringify(data);
    return btoa(data);
  }

  decode(data: string): any {
    return JSON.parse(atob(data));
  }

  getDummyData(): DummyData {
    const groups = [] as EmployeeGroup[];
    for (let i = 0; i < 10; i++) {
      groups.push({
        id: i + 1,
        name: `Group ${i + 1}`
      });
    }

    const employees = [
      {
        id: 1,
        username: 'chandra',
        firstName: 'Chandra',
        lastName: 'Sukmagalih Arifin',
        birthDate: new Date(1998, 11, 18),
        email: 'chandrachansa@gmail.com',
        group: groups[0].name,
        basicSalary: 1000000,
        description: new Date(),
        status: 'Status'
      }
    ] as Employee[];

    for (let i = 1; i < 100; i++) {
      const mainEmployee = employees[0];
      employees.push({
        id: mainEmployee.id + i,
        username: `${mainEmployee.username + i}`,
        firstName: `${mainEmployee.firstName} ${i + 1}`,
        lastName: `${mainEmployee.lastName} ${i + 2}`,
        birthDate: new Date(new Date().getFullYear() - i - 1, i % 12, i),
        email: `${(i % 10) + mainEmployee.email}`,
        group: groups[(i * 3) % groups.length].name,
        basicSalary: mainEmployee.basicSalary + i,
        description: new Date(new Date().getFullYear() - i + i, i % 12, i),
        status: `Status ${i}`
      });
    }
    return { groups, employees };
  }
}
