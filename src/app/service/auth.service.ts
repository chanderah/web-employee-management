import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, map, of } from 'rxjs';
import { User } from '../interface/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private authUrl: string = '../../assets/json/auth.json';
  public isAuthenticated = new BehaviorSubject<boolean>(false);

  constructor(private httpClient: HttpClient) {}

  updateLoggedInState(status: boolean) {
    this.isAuthenticated.next(status);
  }

  getLoginState(): Observable<boolean> {
    return this.isAuthenticated.asObservable();
  }

  login(userLogin: User): Observable<boolean> {
    return this.httpClient.get(this.authUrl).pipe(
      map((res) => {
        const users = res as User[];
        const user = users.find((v) => v.username === userLogin.username);
        if (!user || user.password !== userLogin.password) return false;

        this.setUser(user);
        return true;
      })
    );
  }

  logout(): Observable<boolean> {
    localStorage.removeItem('user');
    this.updateLoggedInState(false);
    return of(true);
  }

  getUser() {
    const user = localStorage.getItem('user');
    this.updateLoggedInState(user ? true : false);
    return user ? this.decode(user) : null;
  }

  setUser(user: User) {
    localStorage.setItem('user', this.encode(user));
  }

  encode(data: any): string {
    if (typeof data !== 'string') data = JSON.stringify(data);
    return btoa(data);
  }

  decode(data: string): any {
    return JSON.parse(atob(data));
  }
}
