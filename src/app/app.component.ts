import { CommonModule } from '@angular/common';
import { Component, HostListener, OnInit } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { Router, RouterModule, RouterOutlet } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { AuthService } from './service/auth.service';
import { MockApiService } from './service/mock-api.service';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, MatToolbarModule, MatIconModule, MatFormFieldModule, MatButtonModule, RouterModule],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', '/src/styles.scss']
})
export class AppComponent implements OnInit {
  isMobile!: boolean;
  isLoggedIn!: boolean;

  title: string = 'Employee Management';
  name: string = 'Chandra SA';

  constructor(
    private router: Router,
    private apiService: MockApiService,
    private authService: AuthService
  ) {
    authService.getLoginState().subscribe((authenticated) => {
      this.isLoggedIn = authenticated;
    });
  }

  @HostListener('window:resize')
  getScreenWidth() {
    this.isMobile = window.innerWidth < 1024;
  }

  ngOnInit(): void {
    this.getScreenWidth();
    this.apiService.initData();
  }

  async onClickAuthAction() {
    if (this.isLoggedIn) await lastValueFrom(this.authService.logout());
    this.router.navigateByUrl('/auth/login');
  }

  // isEmpty(data: any) {
  //   const str = JSON.stringify(data);
  //   return !str || str.length === 0 || str === 'null' || str === undefined;
  // }
}
